import AppConstants from '../constants/AppConstants';
import Dispatcher from '../dispatcher/Dispatcher';

const AppActions = {
    increment() {
        Dispatcher.dispatch({
            actionType: AppConstants.ActionTypes.INCREMENT_INDEX,
        });
    },
};

export default AppActions;