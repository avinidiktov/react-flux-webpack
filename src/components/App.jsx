import React from 'react';

import AppActions from '../actions/AppActions';
import AppStore from '../stores/AppStore';

import styles from './App.scss';

class App extends React.Component {
    constructor(props) {
        super(props);

        this.increment = this.increment.bind(this);
        this.onChange = this.onChange.bind(this);

        this.state = AppStore.getState();
        console.log('init state', this.state);

    }

    componentDidMount() {
        AppStore.addChangeListener(this.onChange);
    }

    componentWillUnmount() {
        AppStore.removeChangeListener(this.onChange);
    }

    onChange() {
        this.setState(AppStore.getState());
    }

    increment() {
        AppActions.increment();
    }

    render() {
        console.log('Change', this.state);
        return (
            <button onClick={ this.increment }>+</button> 
        );
    }
}

export default App;
