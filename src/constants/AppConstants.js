const AppConstants = {
    ActionTypes: {
        INCREMENT_INDEX: 'INCREMENT_INDEX',
    },

    Events: {
        CHANGE_EVENT: 'change',
    },
};


export default AppConstants;
