import Dispatcher from '../dispatcher/Dispatcher';
import AppConstants from '../constants/AppConstants';
import EventEmitter from 'events';

let _state = {
    index: 1,
    editing: false,
};

class AppStoreClass extends EventEmitter {
    constructor() {
        super();
        this._state = _state;
    }

    addChangeListener(cb) {
        this.on(AppConstants.Events.CHANGE_EVENT, cb);
    }

    removeChangeListener(cb) {
        this.removeListener(AppConstants.Events.CHANGE_EVENT, cb);
    }

    increment() {
        this._state.index++;
    }

    getState() {
        return this._state;
    }

}

const AppStore = new AppStoreClass();

Dispatcher.register((payload) => {
    switch (payload.actionType) {
        case AppConstants.ActionTypes.INCREMENT_INDEX:
            AppStore.increment();
            AppStore.emit(AppConstants.Events.CHANGE_EVENT);
            break;
        default:
            return true;
    }
});

export default AppStore;