const path = require('path');

const CopyWebpackPlugin = require('copy-webpack-plugin');
const webpack = require('webpack');


module.exports = {
    devtool: "cheap-eval-source-map",
    entry: [
		'babel-polyfill',
		path.join(__dirname, 'src', 'index.jsx'),
	],
    output: {
        path: path.resolve(__dirname, './build'),
        filename: 'app.js',
        publicPath: '/',
    },
    resolve: {
        extensions: ['.js', '.jsx', '.scss', '.css'],
    },
    module:	{
        rules: [
            {
                test: /.jsx?$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                query: {
                    presets: ['es2015', 'react'],
                },
            },
            {
                test: /\.scss$/,
                loaders: ['style-loader', 'css-loader', 'sass-loader'],
            },
        ],
    },
    plugins: [
        new CopyWebpackPlugin([{ from: 'src/index.html' }]),
    ],
};